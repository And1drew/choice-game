const rockImg = document.getElementById("rockImg");
const paperImg = document.getElementById("paperImg");
const scissorsImg = document.getElementById("scissorsImg");
const userScoreBoard = document.getElementById("userScore");
const computerScoreBoard = document.getElementById("computerScore");
const output = document.getElementById("winResult");
let computerScore = 1;
let userScore = 1;

function computerTurn () {
    let computerChoice = (Math.floor(Math.random() * (4 - 1) + 1));
    if (computerChoice === 1)       {computerChoice = "rock"} 
    else if (computerChoice === 2)  {computerChoice = "paper"}
    else if (computerChoice === 3)  {computerChoice = "scissors"}
    console.log(computerChoice + "  Computer Choice");
    return computerChoice;
}

function checkWinConditions (userChoice,computerChoice) {
    if (computerChoice === userChoice){
        output.innerHTML = "It's a Tie!";

    } else if ((computerChoice === "rock")&& (userChoice === "scissors")){
        output.innerHTML = "Compuer wins! Rock beats Scissors"
        computerScoreBoard.innerHTML = computerScore++;

    } else if ((userChoice === "rock")&& (computerChoice === "scissors")){
        output.innerHTML = "User wins! Rock beats Scissors"
        userScoreBoard.innerHTML = userScore++;

    } else if ((computerChoice === "scissors")&& (userChoice === "paper")){
        output.innerHTML = "Compuer wins! Scissors beats paper"
        computerScoreBoard.innerHTML = computerScore++;

    } else if ((userChoice === "scissors")&& (computerChoice === "paper")){
        output.innerHTML = "User wins! scissors beats paper"
        userScoreBoard.innerHTML = userScore++;

    } else if ((computerChoice === "paper")&& (userChoice === "rock")){
        output.innerHTML = "Compuer wins! paper beats rock"
        computerScoreBoard.innerHTML = computerScore++;

    } else if ((userChoice === "paper")&& (computerChoice === "rock")){
        output.innerHTML = "User wins! paper beats rock"
        userScoreBoard.innerHTML = userScore++;
    }
}

rockImg.onclick = function () {
    console.log("rock UserChoice");
    userChoice = "rock";
    let computerChoice = computerTurn();
    checkWinConditions (userChoice,computerChoice);
}
paperImg.onclick = function () {
    console.log("paper UserChoice");
    userChoice = "paper"
    let computerChoice = computerTurn();
    checkWinConditions (userChoice,computerChoice);
}
scissorsImg.onclick = function () {
    console.log("scissors UserChoice");
    userChoice = "scissors"
    let computerChoice = computerTurn();
    checkWinConditions (userChoice,computerChoice);
}